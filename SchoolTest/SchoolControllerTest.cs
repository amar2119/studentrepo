﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StudentDocumentary.CustomExceptions;
using StudentDocumentary.Models;
using StudentDocumentary.Repository;
using StudentDocumentary.Services;
using System.Threading.Tasks;
using StudentDocumentary.Controllers;
using System;
using Microsoft.AspNetCore.Mvc;

namespace SchooklTest
{
    [TestClass]
    class SchoolControllerTest
    {
        private Mock<IschoolServices> _schoolServices;


        private StudentController _studentController;

        public SchoolControllerTest()
        {   
            _schoolServices = new Mock<IschoolServices>();
            _studentController = new StudentController();

        }
        [TestMethod]
        public void AddSchoolTest()
        {
            School school = new School()
            {
                SchoolId = 1,
                SchoolName = "Bapuji",
                    SchoolAddress = "Secundrabad"

            };

            _schoolServices.Setup(e => e.AddSchool(school)).Returns(Task.FromResult(school));

            var result = _studentController.AddSchool(school).Result;
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
                Assert.IsNotNull(result);
            ;
        }


    }
}
