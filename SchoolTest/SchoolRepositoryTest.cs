using Microsoft.VisualStudio.TestTools.UnitTesting;
using StudentDocumentary.Models;
using StudentDocumentary.Repository;
using Microsoft.EntityFrameworkCore;
using Xunit.Sdk;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchooklTest
{
    [TestClass]
    public class SchoolRepositoryUnitTest
    {

        private DbContextOptions<StudentDbFirstContext> _option = new DbContextOptionsBuilder<StudentDbFirstContext>()
             .UseInMemoryDatabase(databaseName: "StudentDocumentary")
             .Options;


        [TestMethod]
        public void AddSchoolTest()
        {
            // Arrange
            School school = new School()
            {
                SchoolId = 4,
                SchoolName = "Bapuji ",
                SchoolAddress = "Chennai"

            };
            //Act

            var mockData = new StudentDbFirstContext(_option);
            var schoolRepository = new SchoolRepository(mockData);
            var result = schoolRepository.AddSchool(school).Result;

            //Assert
            Assert.AreEqual(school, result);
        }

        [TestMethod]
        public void AddTeacherTest()
        {
            List<Teacher> teachers = new List<Teacher>()
         {
             new Teacher ()
             {
                 TeacherId =2,
                 TeacherName="Gayatri",
                 SchoolId =2,
                 SubjectId=2
             },
             new Teacher ()
             {
                 TeacherId = 3,
                 TeacherName ="panendra",
                 SchoolId=3,
                 SubjectId=3
             }
         };
            using (var dbData = new StudentDbFirstContext(_option))
            {
                dbData.Teachers.Add(new Teacher()
                {
                    TeacherId = 3,
                    TeacherName = "panendra",
                    SchoolId = 3,
                    SubjectId = 3
                }
                    );
                dbData.SaveChanges();
            }

        }

        public void DeleteTeacherTest()
        {
            int id = 6;

            Teacher teacher = new Teacher()
            {
                TeacherId = 2002,
                TeacherName = "Gayatri",
                SchoolId = 1,
                SubjectId = 5
            };
            var mock = new StudentDbFirstContext(_option);

            var repository = new SchoolRepository(mock);

            _ = repository.DeleteTeacher(id);
        }

    }
}

