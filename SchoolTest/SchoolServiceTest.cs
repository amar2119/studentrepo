﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StudentDocumentary.CustomExceptions;
using StudentDocumentary.Models;
using StudentDocumentary.Repository;
using StudentDocumentary.Services;
using System.Threading.Tasks;
using StudentDocumentary.Controllers;
using System.Collections.Generic;

namespace SchooklTest
{
    [TestClass]
   public class SchoolServiceTest
    {
        private readonly Mock<IschoolRepository> _schoolRepoMock; 
        private readonly SchoolServices _schoolServices;

        public SchoolServiceTest()
        {
            _schoolRepoMock = new Mock<IschoolRepository>();
           _schoolServices  = new SchoolServices(_schoolRepoMock.Object);
        }

        [TestMethod]
        public void AddSchoolServiceTest()
        {  
            // arrange 
            School school = new School()
            {
                SchoolId = 8,
                SchoolName = "vijaya public school",
                SchoolAddress = "hmt"
            };

            _schoolRepoMock.Setup(moc => moc.AddSchool(school)).Returns(Task.FromResult(school));
            // var schoolService = new SchoolServices(schoolRepoMock.Object);

            // Act
            var result = _schoolServices.AddSchool(school).Result;

            //Assert
            Assert.AreEqual(8, result.SchoolId);
        }

        [TestMethod]
        public void GetSchoolsTest()
        {
            // arrange
            List<School> school = new List<School>()
            {
               new School
               {
                   SchoolId =5,
                   SchoolName ="Creative high School",
                   SchoolAddress="jeedimetla"
               },
               new School
               {
                   SchoolId =5,
                   SchoolName="Kendriya high School",
                   SchoolAddress="hyd"
               }
            };
            _schoolRepoMock.Setup(op => op.GetSchools()).Returns(Task.FromResult(school));

            // Act
            var result = _schoolServices.GetSchools().Result;

            // Assert
            Assert.AreEqual(school.Count, result.Count);
        }
    }
}
