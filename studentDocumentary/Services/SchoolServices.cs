﻿using log4net.Core;
using Microsoft.Extensions.Logging;
using StudentDocumentary.CustomExceptions;
using StudentDocumentary.Models;
using StudentDocumentary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.Services
{
    public class SchoolServices : IschoolServices
    {
        private readonly IschoolRepository _schoolRepository;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;
        private IschoolRepository @object;

        Task IschoolServices.Products => throw new NotImplementedException();

        public SchoolServices(IschoolRepository schoolRepository ,ILogger<SchoolServices> logger)
        {
            _schoolRepository = schoolRepository;
            _logger =logger;
        }

        public SchoolServices(IschoolRepository @object)
        {
            this.@object = @object;
        }

        public async Task<School> AddSchool(School school)
        {
            try
            {
                if (school == null)
                {
                    throw new MissingSchoolDetailsException(" please enter school name");
                }
                return await _schoolRepository.AddSchool(school);
            }
            catch(Exception e)
            {
                Exception exception = (e.InnerException != null) ? e.InnerException : e;
                _logger.LogError(exception.Message);
                throw new Exception(exception.Message);
            }
        }

        public async Task<int> AddTeacher(Teacher teacher)
        {
            try
            {
                if(teacher != null)
                {
                    throw new MissingteacherDetails("please enter teacher detals");
                }
                _logger.LogInformation("Teacher information ADded successfully");
              

                return await _schoolRepository.AddTeacher(teacher);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                throw new Exception(e.Message);
            }
           
        }

        public Task<List<School>> GetSchools()
        {
            try
            {
                return _schoolRepository.GetSchools();
            }
            catch(Exception e)
            {
                Exception exception = (e.InnerException != null) ? e.InnerException : e;
                _logger.LogError(e.Message);

                throw new Exception(e.Message);
            }
        }

      Task<List<string>> IschoolServices.GetTeacherListbasedOnSchool(string name)
        {
            try
            {
                return _schoolRepository.GetTeacherListbasedOnSchool(name);
            }
           catch(Exception e)
            {
                _logger.LogError(e.Message);
                Exception exception = (e.Message != null) ? e.InnerException : e;
                throw new Exception(e.Message);
            }
        }

        Task IschoolServices.UpdateSchoolAddress(School school)
        {
            try
            {
                if (school == null  )
                {
                    throw new MissingSchoolDetailsException("school name is null, enter the name");
                }
                _logger.LogInformation("update done successfully");
                return _schoolRepository.UpdateSchoolAddress(school);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                throw new Exception(e.Message);
            }
            
        }

        public async Task<int> DeleteTeacher(int id)
        {
            try
            {
                if (id == null)
                {
                    throw new MissingTeachersDetailsException("Teacher is null , enter the name");
                }
                _logger.LogInformation("Teacher removed from database");
                return await _schoolRepository.DeleteTeacher(id);
            }
            catch(Exception e)
            {
                _logger.LogError(e.Message);
                throw new Exception(e.Message);
            }
           
        }
    }
}
