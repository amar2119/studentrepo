﻿using StudentDocumentary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.Services
{
   public interface IschoolServices
    {
        Task Products { get; }

        Task<School> AddSchool(School school);

                Task<int> AddTeacher(Teacher teacher);
                Task<List<School>> GetSchools();
                Task<List<string>> GetTeacherListbasedOnSchool(string name);
               Task   UpdateSchoolAddress(School school);

       // Task<School> UpdateSchoolAddress(int id, School school);

        Task<int> DeleteTeacher(int Id);






    }
}
