﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StudentDocumentary.Models
{
    public partial class Subject
    {
        public Subject()
        {
            Teachers = new HashSet<Teacher>();
        }

        public int SubjectId { get; set; }
        public string SubjectName { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
