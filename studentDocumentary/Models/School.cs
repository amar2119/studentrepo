﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StudentDocumentary.Models
{
    public partial class School
    {
        public School()
        {
            Teachers = new HashSet<Teacher>();
        }

        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAddress { get; set; }

        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
