﻿using System;
using System.Collections.Generic;

#nullable disable

namespace StudentDocumentary.Models
{
    public partial class Teacher
    {
        public int TeacherId { get; set; }
        public string TeacherName { get; set; }
        public int SubjectId { get; set; }
        public int SchoolId { get; set; }

        public virtual School School { get; set; }
        public virtual Subject Subject { get; set; }
    }
}
