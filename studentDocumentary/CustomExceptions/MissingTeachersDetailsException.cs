﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.CustomExceptions
{
    public class MissingTeachersDetailsException : Exception
    {
        public MissingTeachersDetailsException(string message) : base(message)
        {
        }

    }
}
