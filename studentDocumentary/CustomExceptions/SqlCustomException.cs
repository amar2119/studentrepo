﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.CustomExceptions
{
    public class SqlCustomException : Exception
    {
        public SqlCustomException(string message, Exception innerException) : base(message, innerException)
        {
        }

       

    }

}
