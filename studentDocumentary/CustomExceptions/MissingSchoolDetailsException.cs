﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.CustomExceptions
{
    public class MissingSchoolDetailsException : Exception
    {
        public MissingSchoolDetailsException(string message) : base(message)
        {
        }

        public MissingSchoolDetailsException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
