﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentDocumentary.CustomExceptions
{
    public class MissingteacherDetails : Exception
    {
        public MissingteacherDetails(string message) : base(message)
        {

        }
      
    }
}
