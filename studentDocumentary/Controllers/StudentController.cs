﻿using AngleSharp.Css;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Diagnostics.Runtime;
using Microsoft.Extensions.Caching.Memory;
using StudentDocumentary.Models;
using StudentDocumentary.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Priority = FakeItEasy.Priority;

namespace StudentDocumentary.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        private readonly IschoolServices _schoolService;
        private readonly IMemoryCache _memoryCache;

        //public CacheItemPriority Priority { get; private set; }
        //public DateTime AbsoluteExpiration { get; private set; }

        private int Size;
        private CacheItemPriority Priority;

        public DateTime AbsoluteExpiration { get; private set; }

        public StudentController(IschoolServices schoolServices ,IMemoryCache memoryCache)
        {
            _schoolService = schoolServices;
            _memoryCache = memoryCache;



        }

        [HttpPost("Addschool")]
        public async Task<ActionResult<School>> AddSchool(School school)
        {
            try
            {
                var data = await _schoolService.AddSchool(school);
                return data;
            }
            catch(Exception)
            {
                return BadRequest();
            }
        }


        [HttpPost("addteacher")]
        public async Task<IActionResult> AddTeacher(Teacher teacher)
        {
            try
            {
                var data = await _schoolService.AddTeacher(teacher);

                if (data > 0)
                {
                    return Ok("Data was  inserted");

                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("GetSchools")]
        public async Task<IActionResult> GetSchools()
        {
            if (!_memoryCache.TryGetValue("Schooldata", out List<Teacher> data))
            {
                List<School> school = await _schoolService.GetSchools();
                data = school;
                var cacheOptions = new MemoryCacheEntryOptions()
                {
                    Priority = CacheItemPriority.High,
                    AbsoluteExpiration = DateTime.Now
                    .AddMinutes(10),
                    Size = 128
                ;
                _memoryCache.Set("Schooldata", data, cacheOptions);
                //if (data != null)
                //{
                //    return Ok(data);
                //}
                //else
                //{
                //    return NotFound($"school not found");
                //}


                return Ok(data);

            }
        }

        private IActionResult Ok(Func<Task<IActionResult>> getSchools)
        {
            throw new NotImplementedException();
        }

        [HttpGet("GetTeacherListBySchoolName")]
        public async Task<IActionResult> GetTeacherListbasedOnSchool(string name)
        {
            var data = await _schoolService.GetTeacherListbasedOnSchool(name);
            return Ok(data);
        }

        [HttpPut("UpdateSchoolAddress")]
        public async Task<IActionResult> UpdateSchoolAddress(School school)
        {
            try
            {
                await _schoolService.UpdateSchoolAddress(school);
                return Ok("Data was  inserted");
            }
            catch(Exception e)
            {
                return NotFound(e.Message);
            }

        }


        [HttpDelete("DeleteTeacher")]
        public async Task<IActionResult> DeleteTeacher(int Id)
        {

            await _schoolService.DeleteTeacher(Id);
            return Ok("Teacher deleted");
        }
           
    }
}











