﻿using Microsoft.Data.SqlClient;
using StudentDocumentary.CustomExceptions;
using StudentDocumentary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StudentDocumentary.Models;
using Microsoft.EntityFrameworkCore;

namespace StudentDocumentary.Repository
{
    public class SchoolRepository : IschoolRepository
    {
        private readonly StudentDbFirstContext _studentDbFirstContext;

        public SchoolRepository(StudentDbFirstContext studentDbFirstContext)
        {
            _studentDbFirstContext = studentDbFirstContext;
        }

        public async Task<School> AddSchool(School school)
        {
            try
            {
                var Context = await _studentDbFirstContext.Schools.AddAsync(school);
                await _studentDbFirstContext.SaveChangesAsync();
                return Context.Entity;
            }
            catch (SqlException e)
            {
                throw new SqlCustomException("it cannot connect to database", e);
            }
        }

        public async Task<int> AddTeacher(Teacher teacher)
        {
            try
            {
                var context = await _studentDbFirstContext.Teachers.AddAsync(teacher);
                await _studentDbFirstContext.SaveChangesAsync();
                return teacher.TeacherId;
            }
            catch (SqlException ex)
            {
                throw new SqlCustomException("Cannot connect to the data base", ex);
            }
        }

        public async Task<List<School>> GetSchools()
        {
            var context = await _studentDbFirstContext.Schools.ToListAsync();
            return context;
        }

      

      public  async Task<List<string>> GetTeacherListbasedOnSchool(string name)
        {
            return await (from s in _studentDbFirstContext.Schools
                          join T in _studentDbFirstContext.Teachers on
                       s.SchoolId equals T.SchoolId
                          where s.SchoolName == name
                          select T.TeacherName).ToListAsync();
        }

        public async Task UpdateSchoolAddress(School school)
        {
            if (_studentDbFirstContext != null)
            {
                _studentDbFirstContext.Schools.Update(school);
                await _studentDbFirstContext.SaveChangesAsync();
            }
        }
      
        public async Task<int> DeleteTeacher(int Id)
        {
            int result = 0;

            if (_studentDbFirstContext != null)
            {
                var data = await _studentDbFirstContext.Teachers.FirstOrDefaultAsync(e => e.TeacherId == Id);

                if (data != null)
                {
                    _studentDbFirstContext.Teachers.Remove(data);

                    result = await _studentDbFirstContext.SaveChangesAsync();
                }
                return result;
            }
            return result;
        }
    }
}

        


